###################  PROJECTS  #######################

# create a new folder
resource "google_folder" "folder" {
  display_name = var.folder_display_name
  parent     = "organizations/${var.org_id}"
} 


# create network host project
module "create_host_project" {
  source  = "terraform-google-modules/project-factory/google"
  version = "4.0.1"
  billing_account = "${var.billing_account}"
  name = "${var.host_project_name}"
  org_id = "${var.org_id}"
  folder_id = google_folder.folder.name

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
  ]
}

# create ops-asm project
module "create_ops_asm_project" {
  source  = "terraform-google-modules/project-factory/google"
  version = "4.0.1"
  billing_account = "${var.billing_account}"
  name = "${var.ops_project_name}"
  org_id = "${var.org_id}"
  folder_id = google_folder.folder.name
  shared_vpc = module.create_vpc_in_host_project.svpc_host_project_id
  shared_vpc_subnets = [
      "projects/${module.create_vpc_in_host_project.svpc_host_project_id}/regions/${var.subnet_01_region}/subnetworks/${var.subnet_01_name}",
      "projects/${module.create_vpc_in_host_project.svpc_host_project_id}/regions/${var.subnet_02_region}/subnetworks/${var.subnet_02_name}",
  ] 

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
    "cloudbuild.googleapis.com",
    "sourcerepo.googleapis.com",
    "containeranalysis.googleapis.com",
  ]
}

# create dev1 project
module "create_dev1_project" {
  source  = "terraform-google-modules/project-factory/google"
  version = "4.0.1"
  billing_account = "${var.billing_account}"
  name = "${var.dev1_project_name}"
  org_id = "${var.org_id}"
  folder_id = google_folder.folder.name
  shared_vpc = module.create_vpc_in_host_project.svpc_host_project_id
  shared_vpc_subnets = [
      "projects/${module.create_vpc_in_host_project.svpc_host_project_id}/regions/${var.subnet_03_region}/subnetworks/${var.subnet_03_name}",
  ] 

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
  ]
}

# create dev2 project
module "create_dev2_project" {
  source  = "terraform-google-modules/project-factory/google"
  version = "4.0.1"
  billing_account = "${var.billing_account}"
  name = "${var.dev2_project_name}"
  org_id = "${var.org_id}"
  folder_id = google_folder.folder.name
  shared_vpc = module.create_vpc_in_host_project.svpc_host_project_id
  shared_vpc_subnets = [
      "projects/${module.create_vpc_in_host_project.svpc_host_project_id}/regions/${var.subnet_04_region}/subnetworks/${var.subnet_04_name}",
  ] 

  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
  ]
}

