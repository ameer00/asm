# Download istio
resource "null_resource" "exec_download_istio" {
  provisioner "local-exec" {
    command = <<EOT
    wget https://github.com/istio/istio/releases/download/"${var.istio_version}"/istio-"${var.istio_version}"-linux.tar.gz
    tar -xzf istio-"${var.istio_version}"-linux.tar.gz
    rm -r istio-"${var.istio_version}"-linux.tar.gz
    EOT
  }
}