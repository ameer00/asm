# Create certs - This script creates a new certs folder in the current folder and creates the fout required certs for Istio multicluster setup
resource "null_resource" "exec_make_certs" {
  provisioner "local-exec" {
    command = <<EOT
    ./makecerts.sh
    EOT
  }
}