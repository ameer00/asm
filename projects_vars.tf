# Projects and orgs
    variable "billing_account" {}
    variable "org_id" {}

    variable "folder_display_name" {}

    variable "host_project_name" {}
    variable "ops_project_name" {}
    variable "dev1_project_name" {}
    variable "dev2_project_name" {}
