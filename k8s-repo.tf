# Create CSR repo for k8s manifests
resource "google_sourcerepo_repository" "k8s_repo" {
  name = var.k8s_repo_name
  project = module.create_ops_asm_project.project_id
}

# Create a folder structure for k8s manifests
# Copy istio-system cnamespace and CRDs into each GKE cluster folder
resource "null_resource" "exec_create_k8s_repo" {
  provisioner "local-exec" {
    command = <<EOT
    mkdir -p "${google_sourcerepo_repository.k8s_repo.name}"
    cp cloudbuild.yaml "${google_sourcerepo_repository.k8s_repo.name}"/.
    mkdir -p k8s-repo/"${module.create_gke_1_ops_asm_subnet_01.name}"
    mkdir -p k8s-repo/"${module.create_gke_2_ops_asm_subnet_02.name}"
    mkdir -p k8s-repo/"${module.create_gke_1_dev1_r1a_subnet_03.name}"
    mkdir -p k8s-repo/"${module.create_gke_2_dev1_r1b_subnet_03.name}"
    mkdir -p k8s-repo/"${module.create_gke_3_dev2_r2a_subnet_04.name}"
    mkdir -p k8s-repo/"${module.create_gke_4_dev2_r2b_subnet_04.name}"
    cp istio-"${var.istio_version}"/install/kubernetes/namespace.yaml k8s-repo/"${module.create_gke_1_ops_asm_subnet_01.name}"/00_namespace.yaml
    cp istio-"${var.istio_version}"/install/kubernetes/namespace.yaml k8s-repo/"${module.create_gke_2_ops_asm_subnet_02.name}"/00_namespace.yaml
    cp istio-"${var.istio_version}"/install/kubernetes/namespace.yaml k8s-repo/"${module.create_gke_1_dev1_r1a_subnet_03.name}"/00_namespace.yaml
    cp istio-"${var.istio_version}"/install/kubernetes/namespace.yaml k8s-repo/"${module.create_gke_2_dev1_r1b_subnet_03.name}"/00_namespace.yaml
    cp istio-"${var.istio_version}"/install/kubernetes/namespace.yaml k8s-repo/"${module.create_gke_3_dev2_r2a_subnet_04.name}"/00_namespace.yaml
    cp istio-"${var.istio_version}"/install/kubernetes/namespace.yaml k8s-repo/"${module.create_gke_4_dev2_r2b_subnet_04.name}"/00_namespace.yaml
    kubectl create secret generic -n istio-system \
    --from-file=certs/ca-cert.pem \
    --from-file=certs/ca-key.pem \
    --from-file=certs/root-cert.pem \
    --from-file=certs/cert-chain.pem \
    --dry-run cacerts -oyaml > 01_istio-cacerts.yaml
    echo $(ls -d "${google_sourcerepo_repository.k8s_repo.name}"/*/) | xargs -n 1 cp -r istio-"${var.istio_version}"/install/kubernetes/helm/istio-init/files/.
    echo $(ls -d "${google_sourcerepo_repository.k8s_repo.name}"/*/) | xargs -n 1 cp 01_istio-cacerts.yaml
    cat > "${google_sourcerepo_repository.k8s_repo.name}"/README.md << EOF
    This is where the k8s manifests live.
    EOT
  }
  depends_on = [
    null_resource.exec_download_istio, 
    null_resource.exec_make_cloudbuild_yaml,
    null_resource.exec_create_kubeconfig,
  ]
}


# Add files to repo
resource "null_resource" "exec_initial_commit_k8s_repo" {
  provisioner "local-exec" {
    command = <<EOT
    cd "${google_sourcerepo_repository.k8s_repo.name}"
    git config --global user.email $(gcloud auth list --filter=status:ACTIVE --format='value(account)')
    git config --global user.name "terraform"
    git config --global credential.'https://source.developers.google.com'.helper gcloud.sh
    git init
    git remote add google "https://source.developers.google.com/p/${module.create_ops_asm_project.project_id}/r/${google_sourcerepo_repository.k8s_repo.name}"
    git add . && git commit -am "initial"
    git push google master
    EOT
  }
  depends_on=[null_resource.exec_create_k8s_repo,]
}